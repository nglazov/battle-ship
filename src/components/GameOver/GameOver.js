import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';

import './GameOver.css';

const gameOver_ = b_.with('game-over');

export const GameOver = ({visibility, resetProgress}) => {
    if (!visibility) {
        return null;
    }

    return (
        <div className={gameOver_()}>
            <h1>GameOver</h1>
            <a
                onClick={resetProgress}
                className={gameOver_('reset-progress')}
            >
                reset progress
            </a>
        </div>
    );
};

GameOver.propTypes = {
    visibility: PropTypes.bool,
    resetProgress: PropTypes.func
};
