import React, {Component} from 'react';
import _random from 'lodash/random';
import _flatten from 'lodash/flatten';

import {Field} from '../Field/Field';
import {getField, getNeighborhoodCells} from '../../utils/getField';

import './App.css';
import {CELL_TRANSFORMATION, CELL_TYPES} from '../../constants';
import {Controls} from '../Controls/Controls';

class App extends Component {
    constructor() {
        super();

        this.state = {
            ...this._getInitialState()
        }
    }

    _getInitialState = () => ({
        ...getField(),
        gameOver: false
    });

    resetProgress = () => {
        this.setState({
            ...this._getInitialState()
        })
    };

    fire = (to = -1) => {
        const destination = parseInt(to !== -1 ? to : this._getRandomDestination(), 10);
        if (destination === -1) {
            return;
        }

        this._changeCell(destination);

        this._checkShipDestroying(destination);
    };

    _checkShipDestroying = (destination) => {
        const ship = this._getShipByCell(destination);

        if (ship && this._willShipDestroyByFire(ship, destination)) {
            _flatten(ship.map(getNeighborhoodCells)).forEach(this._changeCell);
            this._checkGameOver();
        }
    };

    _getRandomDestination = () => {
        const {field} = this.state;
        const notFired = Object.keys(field)
            .filter((cell) => field[cell] !== CELL_TYPES.MISS && field[cell] !== CELL_TYPES.SUNKEN);

        return notFired.length === 0 ? -1 : notFired[_random(notFired.length - 1)];
    };

    _changeCell = (cellId) => {
        const {field} = this.state;
        const cellType = field[cellId];
        const result = App._getNewCellValue(cellType);

        this.setState((prevState)=>({
            field: {
                ...prevState.field,
                [cellId]: result
            }
        }))
    };

    static _getNewCellValue = (cellType) => {
        return CELL_TRANSFORMATION[cellType] || cellType;
    };

    _getShipByCell = (cellId) => {
        const {ships} = this.state;

        return ships.find((ship) => {
            return ship.includes(cellId)
        });
    };

    _willShipDestroyByFire = (ship, cellId) => {
        const {field} = this.state;

        return ship.every((cell) => {
            return (cell !== cellId && field[cell] === CELL_TYPES.SUNKEN) || cell === cellId;
        });
    };

    _checkGameOver = () => {
        this.setState(({ships, field})=>({
            gameOver: _flatten(ships).every(cellId => {
                return field[cellId] === CELL_TYPES.SUNKEN
            })
        }))
    };

    render() {
        const {gameOver} = this.state;

        return (
            <div>
                <Field
                    field={this.state.field}
                    fire={this.fire}
                    gameOver={gameOver}
                    resetProgress={this.resetProgress}
                />
                <Controls fire={this.fire} gameOver={gameOver}/>
            </div>
        );
    }
}

export default App;
