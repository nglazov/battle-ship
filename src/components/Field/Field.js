import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';

import {Cell} from '../Cell/Cell';

import './Field.css';
import {GameOver} from '../GameOver/GameOver';

const field_ = b_.with('field');

export const Field = ({field = [], fire, gameOver, resetProgress}) => {
    return (
        <div className={field_()}>
            {Object.keys(field).map((key)=>(
                <Cell
                    type={field[key]}
                    key={key}
                    handleClick={fire.bind(null, key)}
                />
            ))}
            <GameOver
                visibility={gameOver}
                resetProgress={resetProgress}
            />
        </div>
    );
};

Field.propTypes = {
    field: PropTypes.object,
    fire: PropTypes.func,
    gameOver: PropTypes.bool,
    resetProgress: PropTypes.func
};
