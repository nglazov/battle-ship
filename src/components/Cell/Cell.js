import React from 'react';
import PropTypes from 'prop-types';
import b_ from 'b_';

import {CELL_TYPES} from '../../constants';

import './Cell.css';

const cell_ = b_.with('cell');

export const Cell = ({type = CELL_TYPES.EMPTY, handleClick}) => {
    return (
        <div
            className={cell_({type: type.toLowerCase()})}
            onClick={handleClick}
        />
    );
};

Cell.propTypes = {
    type: PropTypes.oneOf(Object.keys(CELL_TYPES)),
    handleClick: PropTypes.func
};
