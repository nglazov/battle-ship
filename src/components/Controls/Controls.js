import React from 'react';
import PropTypes from 'prop-types';

import {AUTO_FIRE_INTERVAL} from '../../constants';

export class Controls extends React.Component {
    state = {
        autoFire: false
    };

    handleFireClick = () => {
        this.props.fire();
    };

    toggleAutoFire = () => {
        const {autoFire} = this.state;

        if (!autoFire) {
            this._timer = setInterval(this.props.fire, AUTO_FIRE_INTERVAL)
        }

        if (autoFire) {
            clearInterval(this._timer);
        }

        this.setState({
            autoFire: !autoFire
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.gameOver
            && !this.props.gameOver
            && this.state.autoFire) {
            this.toggleAutoFire();
        }
    }

    render() {
        if (this.props.gameOver) {
            return null;
        }

        const autoFireText = this.state.autoFire ? 'stop auto fire' : 'start auto fire';

        return (
            <div>
                <button onClick={this.toggleAutoFire}>{autoFireText}</button>
                <button onClick={this.handleFireClick}>Fire</button>
            </div>
        );
    }
}


Controls.propTypes = {
    fire: PropTypes.func,
    gameOver: PropTypes.bool
};
