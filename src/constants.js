export const AUTO_FIRE_INTERVAL = 200;

export const FIELD_SIZE = 10;

export const CELL_TYPES = {
    EMPTY: 'EMPTY',
    SHIP: 'SHIP',
    MISS: 'MISS',
    SUNKEN: 'SUNKEN'
};

export const CELL_TRANSFORMATION = {
    [CELL_TYPES.EMPTY]: CELL_TYPES.MISS,
    [CELL_TYPES.SHIP]: CELL_TYPES.SUNKEN
};

export const SHIP_TYPES = {
    DOT: 'DOT',
    I: 'I',
    L: 'L'
};

// each value in array describes offset in one-dimensional array from last point
export const SHIP_FORMS = {
    [SHIP_TYPES.DOT]: [[]],
    [SHIP_TYPES.I]: [[1, 1, 1], [10, 10, 10]],
    [SHIP_TYPES.L]: [
        [10, 10, 1], // L,
        [1, 1, 8], //L 90deg,
        [1, 10, 10], //L 180deg,
        [8, 1, 1] //L 270deg
    ]
};

export const SHIP_COUNTS = {
    [SHIP_TYPES.DOT]: 2,
    [SHIP_TYPES.I]: 1,
    [SHIP_TYPES.L]: 1,
};

export const SHIP_ORDER = [SHIP_TYPES.L, SHIP_TYPES.I, SHIP_TYPES.DOT];
