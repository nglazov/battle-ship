import {checkNeighborhood, getNeighborhoodCells} from './getField';
import {CELL_TYPES} from '../constants';


describe('checkNeighborhood', function () {
    it('returns true on ship in empty field', () => {
        const emptyField = new Array(100).fill(CELL_TYPES.EMPTY);

        expect(checkNeighborhood([0], emptyField)).toBe(true);
    })

    it('returns false on ship in filled field', () => {
        const fullField = new Array(100).fill(CELL_TYPES.SHIP);

        expect(checkNeighborhood([0], fullField)).toBe(false);
    })

    it ('returns false on ship in around', () => {
        const customField = new Array(100).fill(CELL_TYPES.EMPTY);
        customField[45] = CELL_TYPES.SHIP;

        expect(checkNeighborhood([34], customField)).toBe(false);
        expect(checkNeighborhood([35], customField)).toBe(false);
        expect(checkNeighborhood([36], customField)).toBe(false);
        expect(checkNeighborhood([44], customField)).toBe(false);
        expect(checkNeighborhood([46], customField)).toBe(false);
        expect(checkNeighborhood([54], customField)).toBe(false);
        expect(checkNeighborhood([55], customField)).toBe(false);
        expect(checkNeighborhood([56], customField)).toBe(false);
    });
});

describe('getNeighborhoodCells', ()=>{
    it('returns [1, 10, 11] for 0', ()=>{
      expect(getNeighborhoodCells(0).sort()).toEqual([1, 10, 11].sort())
    });

    it('returns [0, 1, 11, 20, 21] for 10', ()=>{
        expect(getNeighborhoodCells(10).sort()).toEqual([0, 1, 11, 20, 21].sort())
    });

    it('returns [34, 35, 36, 44, 46, 54, 55, 56] for 10', ()=>{
        expect(getNeighborhoodCells(45).sort()).toEqual([34, 35, 36, 44, 46, 54, 55, 56].sort())
    });
})
