import {CELL_TYPES, FIELD_SIZE, SHIP_COUNTS, SHIP_FORMS, SHIP_ORDER} from '../constants';
import _random from 'lodash/random';
import _flatten from 'lodash/flatten';

export function getField() {
    const newField = Array(FIELD_SIZE * FIELD_SIZE).fill(CELL_TYPES.EMPTY);
    const ships = [];

    SHIP_ORDER.forEach((type) => {
        for (let i = 0; i < SHIP_COUNTS[type]; i++) {
            const ship = getShipPosition(type, newField);
            ships.push(ship);

            ship.forEach((val) => {
                newField[val] = CELL_TYPES.SHIP
            });
        }
    });

    return {
        field: newField.reduce((sum, val, key) => {
            sum[key] = val;
            return sum;
        }, {}),
        ships
    };
}

function getShipPosition(shipType, field) {
    let result = [];

    while (result.length < 1) {
        const start = _random(0, 99);

        result = (SHIP_FORMS[shipType] || [[]])
            .map(calculatePositionByScheme(start))
            .filter(isShipValid(field))
            .map(({ship}) => ship);
    }

    return result[_random(result.length - 1)];
}

function calculatePositionByScheme(start) {
    return function (scheme) {
        return scheme.reduce((sum, val, i) => {
            sum.ship.push(val + sum.ship[i]);
            return sum
        }, {scheme, ship: [start]})
    }
}

function isShipValid(field) {
    return ({ship, scheme}) => {
        if (!isValidByBounds(ship)) {
            return false
        }

        if (!isValidByScheme(ship, scheme)) {
            return false
        }

        if (!isValidByPlace(ship, field)) {
            return false
        }

        return true;
    }
}

function haveNextRow(a, b) {
    return String(a).padStart(2, '0').slice(0, 1) !== String(b).padStart(2, '0').slice(0, 1)
}

function haveSameRows(a, b) {
    return String(a).padStart(2, '0').slice(0, 1) === String(b).padStart(2, '0').slice(0, 1)
}

function isValidByBounds(ship) {
    if (ship.some((val) => val < 0)) {
        return false;
    }

    if (ship.some((val) => val > 99)) {
        return false;
    }

    return true
}

function isValidByScheme(ship, scheme) {
    return scheme.every((step, i) => {
        let f = haveNextRow;
        if (step === 1) {
            f = haveSameRows
        }

        return f(ship[i], ship[i + 1])
    });
}

function isValidByPlace(ship, field) {
    if (!ship.every(checkPlace(field))) {
        return false;
    }

    if (!checkNeighborhood(ship, field)) {
        return false;
    }

    return true;
}

export function checkNeighborhood(ship, field) {
    return _flatten(ship.map(getNeighborhoodCells))
        .every(checkPlace(field))
}

function checkPlace(field) {
    return (place) => {
        return field[place] === CELL_TYPES.EMPTY
    }
}

export function getNeighborhoodCells(cell) {
    const [y, x] = String(cell).padStart(2, 0).split('');
    const points = [];

    if (x !== '0' && y !== '0') {
        points.push('nw');
    }

    if (y !== '0') {
        points.push('n');
    }

    if (x !== '9' && y !== '0') {
        points.push('ne');
    }

    if (x !== '9') {
        points.push('e');
    }

    if (x !== '9' && y !== '9') {
        points.push('se');
    }

    if (y !== '9') {
        points.push('s');
    }

    if (y !== '9' && x !== '0') {
        points.push('sw');
    }

    if (x !== '0') {
        points.push('w')
    }

    return points.map(direction => cell + POINTS_TO_CHECK[direction])
}

export const POINTS_TO_CHECK = {
    nw: -11,
    n: -10,
    ne: -9,
    w: -1,
    e: 1,
    sw: 9,
    s: 10,
    se: 11
};
